use crate::map::Point;

use super::node::Distance;

#[derive(Debug, Default)]
pub struct Path {
    pub trace: Vec<Point>,
    pub distance: Distance,
}
