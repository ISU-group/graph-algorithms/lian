use std::{cmp::Ordering, rc::Rc};

use crate::map::Point;

pub(crate) type Distance = f64;

#[derive(Debug)]
pub(crate) struct Node {
    pub(crate) distance: Distance,
    pub(crate) point: Point,
    pub(crate) parent: Option<Rc<Node>>,
}

impl Node {
    pub fn rc(distance: Distance, point: Point, parent: Option<Rc<Node>>) -> Rc<Self> {
        Rc::new(Self {
            distance,
            point,
            parent,
        })
    }
}

impl Eq for Node {}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        if self.point != other.point {
            return false;
        }

        match (&self.parent, &other.parent) {
            (None, None) => true,
            (Some(lhs), Some(rhs)) => lhs.point == rhs.point,
            _ => false,
        }
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let ord = self.point.cmp(&other.point);

        if ord.is_ne() {
            return Some(ord);
        }

        self.parent
            .as_ref()?
            .point
            .partial_cmp(&other.parent.as_ref()?.point)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeSet;

    use super::*;

    #[test]
    fn new_test() {
        let distance = 1.0;
        let point = Point::new(1, 1);
        let parent = Point::new(0, 0);

        let parent_node = Node::rc(0.0, parent, None);
        let node = Node::rc(distance, point, Some(parent_node));

        assert_eq!(distance, node.distance);
        assert_eq!(Point::new(1, 1), node.point);
        assert_eq!(Point::new(0, 0), node.parent.as_ref().unwrap().point);
    }

    #[test]
    fn eq_test() {
        let node1 = Node::rc(1.0, Point::new(0, 0), None);
        let node2 = Node::rc(1.0, Point::new(0, 0), None);

        assert_eq!(node1, node2);

        let node3 = Node::rc(5.0, Point::new(3, 3), Some(node1));
        let node4 = Node::rc(5.0, Point::new(3, 3), Some(node2));

        assert_eq!(node3, node4);
    }

    #[test]
    fn btree_set_test() {
        let root = Node::rc(0.0, Point::new(0, 0), None);

        let set = BTreeSet::from([
            root.clone(),
            Node::rc(1.0, Point::new(1, 1), Some(root.clone())),
            Node::rc(2.0, Point::new(0, 0), None),
            Node::rc(1.0, Point::new(2, 2), Some(root.clone())),
        ]);

        let node1 = Node::rc(5.0, Point::new(1, 1), Some(root.clone()));
        let node2 = Node::rc(1.0, Point::new(2, 2), Some(root.clone()));
        let node3 = Node::rc(5.0, Point::new(0, 2), None);

        assert!(set.contains(&node1));
        assert!(set.contains(&node2));
        assert!(!set.contains(&node3));
        assert_eq!(3, set.len());
    }
}
