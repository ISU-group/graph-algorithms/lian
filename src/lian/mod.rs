pub use self::{lian_params::LianParams, path::Path};

use crate::map::{Map, Point};

use self::lian::Lian;

mod lian;
mod lian_params;
mod node;
mod path;

pub fn get_optimal_path(map: Map, params: LianParams, start: Point, goal: Point) -> Option<Path> {
    let mut lian = Lian::new(map, params);
    let mut path = lian.get_optimal_path(&start, &goal)?;

    let mut full_trace = Vec::default();
    for i in 0..path.trace.len() - 1 {
        let mut line = bresenham_line(&lian.map, &path.trace[i + 1], &path.trace[i])
            .expect("Undefined exception: line is None (not supposed to be)");
        full_trace.append(&mut line);
    }

    path.trace = full_trace;
    Some(path)
}

pub fn bresenham_line(map: &Map, start: &Point, goal: &Point) -> Option<Vec<Point>> {
    let mut x1 = start.x;
    let mut y1 = start.y;
    let x2 = goal.x;
    let y2 = goal.y;

    let delta_x = x2.abs_diff(x1) as isize;
    let delta_y = y2.abs_diff(y1) as isize;

    let sign_x: isize = if x1 < x2 { 1 } else { -1 };
    let sign_y: isize = if y1 < y2 { 1 } else { -1 };

    let mut error = delta_x - delta_y;

    if !map.contains(start) || map[start].is_untraversable() {
        return None;
    }

    let mut line = Vec::default();

    while x1 != x2 || y1 != y2 {
        let point = Point::new(x1, y1);
        if map[&point].is_untraversable() {
            return None;
        }

        line.push(point);

        let error2 = error * 2;
        if error2 > -delta_y {
            error -= delta_y;
            x1 = x1.wrapping_add_signed(sign_x);
        }
        if error2 < delta_x {
            error += delta_x;
            y1 = y1.wrapping_add_signed(sign_y);
        }
    }

    Some(line)
}
