use std::{cmp::Ordering, collections::BTreeSet, rc::Rc};

use crate::map::{Map, Point};

use super::{
    bresenham_line,
    lian_params::{Degree, LianParams},
    node::{Distance, Node},
    path::Path,
};

pub(crate) struct Lian {
    pub(crate) map: Map,
    open: BTreeSet<Rc<Node>>,
    closed: BTreeSet<Rc<Node>>,
    params: LianParams,
    goal: Point,
    start: Point,
}

const FLOAT_PRECISION: f64 = 0.00002;

impl Lian {
    pub fn new(map: Map, params: LianParams) -> Self {
        Self {
            map,
            params,
            open: Default::default(),
            closed: Default::default(),
            goal: Point::new(0, 0),
            start: Point::new(0, 0),
        }
    }

    pub fn get_optimal_path(&mut self, start: &Point, goal: &Point) -> Option<Path> {
        self.goal = goal.clone();
        self.start = start.clone();
        // dummpy variable not to handle with None in comparison
        let root_parent = Node::rc(0.0, start.clone(), None);

        self.open
            .insert(Node::rc(0.0, start.clone(), Some(root_parent)));

        let mut trace = Vec::default();

        while !self.open.is_empty() {
            let node = self.pop_open_node();
            trace.push(node.point.clone());

            if node.point.clone() == goal.clone() {
                return self.get_node_path(node);
            }

            self.closed.insert(node.clone());
            self.expand(node);
        }

        None
    }

    fn pop_open_node(&mut self) -> Rc<Node> {
        let (_, v) = self
            .open
            .iter()
            .enumerate()
            .min_by(|a, b| {
                let af_value = self.distance(&a.1.point, &self.goal) + a.1.distance;
                let bf_value = self.distance(&b.1.point, &self.goal) + b.1.distance;

                af_value.total_cmp(&bf_value)
            })
            .unwrap();

        self.open.take(&v.clone()).unwrap()
    }

    fn distance(&self, a: &Point, b: &Point) -> Distance {
        ((b.x as Distance - a.x as Distance).powi(2) + (b.y as Distance - a.y as Distance).powi(2))
            .sqrt()
    }

    fn get_node_path(&self, mut node: Rc<Node>) -> Option<Path> {
        node.parent.as_ref()?;

        let mut trace = Vec::from([node.point.clone()]);
        let distance = node.distance;

        while let Some(parent) = node.parent.clone() {
            trace.push(parent.point.clone());
            node = parent;
        }

        Some(Path { trace, distance })
    }

    fn expand(&mut self, node: Rc<Node>) {
        let mut successors = self.get_circle_successors(node.clone());
        let goal_distance = self.distance(&node.point, &self.goal);

        if cmp_approximately(goal_distance, self.params.radius, FLOAT_PRECISION).is_lt() {
            successors.push(Node::rc(
                goal_distance,
                self.goal.clone(),
                Some(node.clone()),
            ));
        }

        for successor in successors {
            if !self.map.contains(&successor.point) || self.map[&successor.point].is_untraversable()
            {
                continue;
            }

            if node.point.ne(&node.parent.as_ref().unwrap().point) {
                if self.closed.contains(&successor) {
                    continue;
                }

                let angle = self.get_angle(&successor, &node);
                if self.has_unacceptable_angle(angle) {
                    continue;
                }
            }

            if !self.has_line_of_sight(&node.point, &successor.point) {
                continue;
            }

            self.open.insert(Node::rc(
                successor.distance + node.distance,
                successor.point.clone(),
                successor.parent.clone(),
            ));
        }
    }

    fn get_circle_successors(&self, node: Rc<Node>) -> Vec<Rc<Node>> {
        self.get_circumference(&node.point)
            .into_iter()
            .map(|point| Node::rc(self.params.radius, point, Some(node.clone())))
            .collect()
    }

    fn get_circumference(&self, center: &Point) -> BTreeSet<Point> {
        let mut x = 0;
        let mut y = self.params.radius as isize;
        let mut decision = 5.0 / 4.0 - self.params.radius;

        let mut circle = BTreeSet::from([(x, y), (x, -y), (y, x), (-y, x)]);

        while y > x {
            x += 1;
            if decision.is_sign_negative() {
                decision += 2.0 * x as f64 + 1.0;
            } else {
                y -= 1;
                decision += 2.0 + (x - y) as f64 + 1.0;
            }

            circle.extend([
                (x, y),
                (x, -y),
                (-x, y),
                (-x, -y),
                (y, x),
                (-y, x),
                (y, -x),
                (-y, -x),
            ]);
        }

        circle
            .iter()
            .filter_map(|(x, y)| {
                let x = center.x.checked_add_signed(*x)?;
                let y = center.y.checked_add_signed(*y)?;

                Some(Point::new(x, y))
            })
            .collect()
    }

    fn get_angle(&self, target: &Rc<Node>, start: &Rc<Node>) -> Degree {
        let (ax, ay) = (
            start.point.x as f64 - start.parent.as_ref().unwrap().point.x as f64,
            start.point.y as f64 - start.parent.as_ref().unwrap().point.y as f64,
        );
        let (bx, by) = (
            target.point.x as f64 - target.parent.as_ref().unwrap().point.x as f64,
            target.point.y as f64 - target.parent.as_ref().unwrap().point.y as f64,
        );

        let al = (ax.powi(2) + ay.powi(2)).sqrt();
        let bl = (bx.powi(2) + by.powi(2)).sqrt();

        ((ax * bx + ay * by) / (al * bl))
            .clamp(-1.0, 1.0)
            .acos()
            .to_degrees()
    }

    fn has_unacceptable_angle(&self, angle: Degree) -> bool {
        cmp_approximately(angle.abs(), self.params.angle, FLOAT_PRECISION).is_gt()
    }

    fn has_line_of_sight(&self, start: &Point, goal: &Point) -> bool {
        bresenham_line(&self.map, start, goal).is_some()
    }
}

fn cmp_approximately(a: f64, b: f64, precision: f64) -> Ordering {
    let a_int = a.trunc() as i64;
    let b_int = b.trunc() as i64;

    let a_fract = a.fract().abs();
    let b_fract = b.fract().abs();

    let ord = a_int.cmp(&b_int);
    if ord.is_ne() {
        return ord;
    }

    (a_fract - b_fract).abs().total_cmp(&precision)
}
