pub(crate) type Radius = f64;
pub(crate) type Degree = f64;

#[derive(Debug, Clone)]
pub struct LianParams {
    pub radius: Radius,
    pub angle: Degree,
}
