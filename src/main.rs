use lian::{self as LIAN, LianParams};
use map::{Map, Point};

mod lian;
mod map;

fn main() {
    let image_path = "karta-01.bmp";
    let mut img = bmp::open(image_path).unwrap();

    let map = Map::from_bmp(&img);

    let start = Point::new(165, 305);
    let goal = Point::new(1290, 690);

    let params = LianParams {
        radius: 5.0,
        angle: 20.0,
    };

    if let Some(path) = LIAN::get_optimal_path(map, params.clone(), start, goal) {
        println!("Path length: {}", path.distance);

        for point in &path.trace {
            img.set_pixel(point.x as u32, point.y as u32, bmp::consts::DARK_RED);
        }

        let filename = format!("angle_{:.2}_r_{:.2}.bmp", params.radius, params.angle);
        img.save(filename).unwrap();
    } else {
        println!("No path found");
    }
}
