use std::ops::Index;

use bmp::Image;

use super::{cell_kind::CellKind, point::Point};

#[derive(Debug)]
pub struct Map {
    data: Vec<CellKind>,
    width: usize,
    height: usize,
}

impl Map {
    pub fn from_bmp(img: &Image) -> Self {
        let width = img.get_width();
        let height = img.get_height();
        let size = (width * height) as usize;

        let mut data = Vec::with_capacity(size);
        data.resize(size, CellKind::Traversable);

        for (x, y) in img.coordinates() {
            let index = (y * width + x) as usize;

            if img.get_pixel(x, y) == bmp::consts::BLACK {
                data[index] = CellKind::Untraversable;
            }
        }

        Self {
            data,
            width: width as usize,
            height: height as usize,
        }
    }

    pub fn contains(&self, point: &Point) -> bool {
        point.x < self.width && point.y < self.height
    }

    fn is_valid_index(&self, index: usize) -> bool {
        index < self.data.len()
    }
}

impl Index<&Point> for Map {
    type Output = CellKind;

    fn index(&self, point: &Point) -> &Self::Output {
        &self[(point.x, point.y)]
    }
}

impl Index<(usize, usize)> for Map {
    type Output = CellKind;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        let index = y * self.width + x;

        if self.is_valid_index(index) {
            return &self.data[index];
        }

        panic!("({}, {}) is out of the range", x, y)
    }
}

// Only for test purposes to create any map
pub trait FromBinary {
    fn from_binary(data: &[Vec<u8>]) -> Self;
}

impl FromBinary for Map {
    fn from_binary(data: &[Vec<u8>]) -> Self {
        let height = data.len();
        assert_ne!(height, 0);

        let width = data[height - 1].len();
        let data = data
            .iter()
            .flatten()
            .map(|&x| {
                if x == 1 {
                    CellKind::Untraversable
                } else {
                    CellKind::Traversable
                }
            })
            .collect();

        Self {
            data,
            width,
            height,
        }
    }
}
