pub use {
    cell_kind::CellKind,
    map::{FromBinary, Map},
    point::Point,
};

mod cell_kind;
mod map;
mod point;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_from_binary() {
        let map_data = vec![
            vec![0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 1, 1, 0, 0, 1, 0, 0],
            vec![0, 0, 1, 0, 0, 0, 1, 0],
            vec![0, 0, 1, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 1, 1, 0, 0],
            vec![0, 0, 0, 0, 1, 1, 0, 0],
            vec![0, 0, 0, 1, 0, 0, 0, 0],
        ];

        let map = Map::from_binary(&map_data);

        for y in 0..map_data.len() {
            for x in 0..map_data[y].len() {
                let cell_kind = if map_data[y][x] == 0 {
                    CellKind::Traversable
                } else {
                    CellKind::Untraversable
                };

                assert_eq!(cell_kind, map[(x, y)])
            }
        }
    }
}
