#[derive(Debug, Clone, PartialEq, Eq)]
pub enum CellKind {
    Traversable,
    Untraversable,
}

impl CellKind {
    pub fn is_traversable(&self) -> bool {
        !self.is_untraversable()
    }

    pub fn is_untraversable(&self) -> bool {
        match *self {
            CellKind::Traversable => false,
            CellKind::Untraversable => true,
        }
    }
}
