# LIAN algorithm
(Rust implementation)

---
Angle: 20.0<br>
Radius: 5.0<br>

Path length: 1197

<img src="results/angle_20_r_5.png">

---

Angle: 45.0<br>
Radius: 5.0<br>

Path length: 1197

<img src="results/angle_45_r_5.png">
